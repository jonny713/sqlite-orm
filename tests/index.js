const { connect, server, Table } = require('./../index.js')
const logger = require('./../utils/log.js')({ lim: 1, showTrace: false })


const testing = async () => {
  // Соединяемся с фалойм базы данных
  try {
    connect()
  } catch (e) {
    console.error(e.message)
  }

  // запускаем сервер
  try {
    server.start('6003')
  } catch (e) {
    console.error(e.message)
  }

  // Создаем таблицу
  let test, refTable
  try {
    refTable = new Table('streamers', {
      id: 'ID',
      name: 'TEXT',
    })


    test = new Table('tests', {
      perks: 'ARRAY',
      isTrue: 'BOOLEAN',
      id: 'ID',
      sort: 'INTEGER',
      store: 'OBJECT',
      streamers: {
        type: 'REFERENCE',
        table: refTable
      },
      name: {
        type: 'TEXT'
      },
    })
  } catch (e) {
    console.error(e)
  }

  // Кладем туда несколько записей
  try {
    console.log('Кладем в таблицу несколько записей')
    await refTable.insert({ name: 'Запервя', })
    console.log(await test.insert({
      id:22,
      perks: ['Красив', 'Весел'],
      isTrue: false,
      sort: 777,
      store: {
        goo: 12,
        foo: '13'
      },
      streamers: 1,
      name: 'Ugluck'
    }))

  } catch (e) {
    console.log(e)
  }

  // // Пробуем положить с корявыми данными
  // try {
  //   console.log('Кладем корявую запись в таблицу')
  //   console.log(await test.insert({
  //     sort: 1,
  //     name: 'Запись тестовая, первая',
  //   }))
  // } catch (e) {
  //   console.error(e.message)
  // }
  // try {
  //   console.log('Кладем корявую запись в таблицу')
  //   console.log(await test.insert({
  //     sort1: 2,
  //     name2: 'Запись тестовая, вторая',
  //   }))
  // } catch (e) {
  //   console.error(e.message)
  // }

  // Достаем их
  try {
    console.log('Достаем их')
    console.log(await test.get())
  } catch (e) {
    console.error(e)
  }

  // Обновляем
  try {
    console.log('Обновляем')
    console.log(await test.update({
      search: {
        sort: 1
      },
      updatedData: {
        name: undefined,
      }
    }))
  } catch (e) {
    console.error(e.message)
  }

  // Проверяем состав
  try {
    console.log('Проверяем состав')
    console.log(await test.get())
  } catch (e) {
    console.error(e.message)
  }


  // Удаляем некоторые
  try {
    console.log('Удаляем некоторые')
    console.log(await test.delete({
      search: {
        sort: 1
      }
    }))
  } catch (e) {
    console.error(e.message)
  }

  // Проверяем состав
  try {
    console.log('Проверяем состав')
    console.log(await test.get())
  } catch (e) {
    console.error(e.message)
  }

}

testing()
