# sqlite-orm

Модуль, выполняющий контроль за sqlite бд, реализующий доступ к данным с использованием схем

# Описание
Надстройка над sqlite3, предоставляющая доступ к БД SQLite в объектном виде с использованием схем


# Подключение

```
const database = require('@sqlite-orm/core')

const dbName = 'filename.db'

// Соединяемся с фалойм базы данных
database.connect({ dbName })
```

Имеется возможность включения сервера с CRUD набором маршрутов, по всем таблицам
```
const port = 5000

// запускаем сервер
database.server.start(db.port)
```

# Использование
Для создания таблицы в БД необходимо описать её схему
```
const { Table } = require('sqlite-orm')


let examples = new Table('examples', {
  id: 'ID',
  sort: {
    type: 'INTEGER',
    notNull: true,
    unique: true
  },
  name: {
    type: 'TEXT',
    notNull: true,
    unique: true
  },
  position: 'TEXT',
  active: {
    type: 'BOOLEAN',
    default: true
  }
})
```
## Описание схемы
```
let examples = new Table(tableName, schema)
```
---
описание схемы

---

## Объект таблицы
Объект таблицы реализует следующие методы
---
описание методов

---

# Запланированные обновления
* тесты
* документация
