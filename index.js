/*
* Реализация класса для доступа к данным sqlite
* создается подключение к указанному файлу базы данных
* поддерживаются методы получения, вставки, изменения и удаления данных
*/
// TODO: добавить метод резервирующий файл бд

const connect = require('./lib/database/connect.js')
const Table = require('./lib/table/schema.js')
const { middlewares, updateMiddlewares } = require('./lib/view/express-middlewares.js')
const { start } = require('./lib/view/server.js')

let database

const proxyTable = new Proxy(Table, {
  construct: function(target, args) {
    //
    if (database === undefined) {
      throw Error('Не указан файл базы данных. Воспользуйтесь sqliteOrm.connect(dbName)')
    }

    const table = new target(database, ...args)

    updateMiddlewares(table)

    return table
  }
})

module.exports = {
  connect: async (dbName) => {
    database = connect(dbName)
    console.log(database)
  },
  Table: proxyTable,
  server: {
    start: (port) => {
      return start(port, middlewares)
    }
  }
}
