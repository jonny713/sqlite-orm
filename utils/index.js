class TableManage {
  /*
  * создать таблицу
  */
  static async add({ table }) {

    // let queue = `CREATE TABLE "${name}" (`
    //
    // for (let c in columns) {
    //   queue += `\n\t"${c}"\t`
    //   // console.log(columns[c])
    //   queue += columns[c].sql()
    // }
    //
    // queue += `)`
    //
    // console.log(queue)
    // return queue
    // let result = await new Promise( (res, rej) =>
    //   db.all(queue, (err, rows) => err ? rej(err) : res(rows) ) )

    let queue = table.sql
    console.log(queue)

    let result = await db.request({ query: queue })

    return result
  }

  /*
  * создать временную таблицу
  */
  static async addTemp(columns) {
    // вызываем метод addTable с фиксированным именем для временных таблиц
    return await Table.add({
      name: TEMP_TABLE_NAME,
      columns
    })
  }

  /*
  * скопировать данные из одной таблицы в другую
  */
  static async copyData() {

  }

  /*
  * удалить таблицу
  */
  static async drop() {

  }

  /*
  * переименовать таблицу
  */
  static async rename(oldName, newName) {

  }


  /**
  * метод для сверения таблицы в базе данных с заданной схемой
  */
  compare(table) {
    if (table.sql !== this.sql) {
      console.info(`Некоторые столбцы ${this.name} изменены. Воспользуйтесь утилитой`)
      console.info('%j', table.columns)
      console.info('%j', this.columns)
      console.info('%s', table.sql)
      console.info('%s', this.sql)

      for (let col in this.columns) {
        let comparableCol = table.columns.find(c => c.name == col)

        // Если в существуюущей таблице нет нужного столбца, стоит его добавить
        if (comparableCol === undefined) {
          comparableCol = {}
        }

        // на текущей реализации первичный ключ поддерживается только для ID.
        // сам формат ID стандартен
        if (this.columns[col] instanceof Table.types['ID']) {
          assert(comparableCol.type === 'INTEGER', `Тип данных id не поддерживается библиотекой. Необходим INTEGER. Проверьте БД, таблица ${this.name}`)
          assert(comparableCol.primaryKey === true, `Для id должен быть выставлен флаг PRIMARY KEY. Проверьте БД, таблица ${this.name}`)
          assert(comparableCol.notNull === true, `Для id должен быть выставлен флаг NOT NULL. Проверьте БД, таблица ${this.name}`)
          assert(comparableCol.autoincrement === true, `Для id должен быть выставлен флаг AUTOINCREMENT. Проверьте БД, таблица ${this.name}`)
          assert(comparableCol.unique === true, `Для id должен быть выставлен флаг UNIQUE. Проверьте БД, таблица ${this.name}`)
          assert(comparableCol.default === undefined, `Для id не должно быть значения DEFAULT. Проверьте БД, таблица ${this.name}`)
          continue
        }

        // Если у столбца в БД primaryKey == true, выдаем ошибку
        if (comparableCol.primaryKey)
          throw Error('Флаг PRIMARY KEY')


        // Если значение флагов разное в схеме и в реальной БД
        if (comparableCol.unique !== this.columns[col].unique ||
          comparableCol.autoincrement !== this.columns[col].autoincrement ||
          comparableCol.notNull !== this.columns[col].notNull ||
          comparableCol.default != this.columns[col].default) {

          // переименовывем старую таблицу

          // создаем новую таблицу согласно схеме

          // пытаемся скопировать записи из старой в новую

          // если получается, сообщаем об успешности

          // если не получается, выдаем ошибку


        }

      }
    }

  }

  /*
  * добавление столбца
  */
  async addColumn(schema = {}) {
    // проходим по всем стобцам из схемы
    for (let col in schema) {
      // если в качестве значения для столбца строка, считаем, что это тип
      if (typeof schema[col] == 'string')
        schema[col] = { type: schema[col] }

      // достаем тип
      let { type } = schema[col]

      // проверяем, что тип поддерживается
      if (Table.types[type] === undefined)
        throw Error(`Неизвестный тип данных ${type} для столбца таблицы. Используйте один из следующих: ${Object.keys(Table.types)}`)

      // проверяем, что столбец новый
      if (this.columns[col] !== undefined)
        throw Error(`Столбец с именем ${col} уже есть в таблице. Список всех столбцов: ${Object.keys(this.columns)}`)

      // добавлем стобец в список
      this.columns[col] = schema[col]
    }

    // создаем техническую таблицу со теми же стобцами, что и в текущей
    let tempTable = await Table.addTemp(this.columns)

    // копируем в нее данные из текущей таблицы
    await Table.copyData({
      from: tempTable,
      to: this.table
    })

    // удаляем текущую таблицу
    await Table.drop({
      from: tempTable,
      to: this.table
    })

    // меняем имя технической таблицы на имя текущей
  }

  /*
  * удаление столбца
  */
  deleteColumn() {

  }

  /*
  * изменение столбца
  */
  updateColumn() {

  }

  constructor() {

  }
}
