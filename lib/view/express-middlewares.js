const express = require('express')
const router = express.Router()


const TABLES = []


function listGetter(table) {
  return async (req, res) => {
    try {
      let list = await table.get()
      res.send(list)
    } catch (e) {
      res.send({ error: e.message })
    }
  }
}
function elementAdder(table) {
  return async (req, res) => {
    try {
      let newElementId = await table.insert(req.body)

      let newElement = await table.getOne({
        id: newElementId
      })
      res.send(newElement)
    } catch (e) {
      console.error(e);
      res.send({ error: 'Запись НЕ добавлена' })
    }
  }
}
function elementFinder(table, propertyName, searchPropertyName) {
  return async (req, res, next) => {
    try {
      req.dbData = req.dbData || {}
      req.dbData[propertyName] = await table.getOne({
        [searchPropertyName]: req.params[propertyName]
      })

      next()
    } catch (e) {
      res.send({ error: e.message })
    }
  }
}
function elementGetter(propertyName) {
  return async (req, res) => {
    try {
      let element = req.dbData[propertyName]

      res.send(element)
    } catch (e) {
      res.send({ error: e.message })
    }
  }
}
function elementUpdater(table, propertyName) {
  return async (req, res) => {
    try {
      let element = req.dbData[propertyName]

      await table.update({
        id: element.id,
        updatedData: req.body
      })

      let updatedElement = await table.getOne({
        id: element.id
      })

      res.send(updatedElement)
    } catch (e) {
      console.error(e);
      res.send({ error: 'Запись НЕ изменена' })
    }
  }
}
function elementDeleter(table, propertyName) {
  return async (req, res) => {
    try {
      let element = req.dbData[propertyName]

      await table.delete({
        id: element.id
      })

      res.send({ status: 'Запись успешно удалена' })
    } catch (e) {
      console.error(e);
      res.send({ status: 'Запись НЕ удалена' })
    }
  }
}
function elementSearcher(table, propertyName) {
  return async (req, res) => {
    try {
      let list = await table.get(req.query)

      res.send(list)
    } catch (e) {
      res.send({ error: e.message })
    }
  }
}


// Лог запроса
router.use((req, res, next) => {
  console.log(`==> ${req.method} ${req.url} ${req.method == 'POST' && Object.keys(req.body).length ? "\n" + JSON.stringify(req.body): ''}`);
  next()
})

// Запрос всех таблиц
router.get('/tables',  (req, res) => {
  res.send(TABLES.map(table => {
    return {
      name: table.name,
      title: table.title,
      headers: table.columns,
      // TODO: дополнительная метаинформация по таблице
    }
  }))
})

module.exports.middlewares = router
module.exports.updateMiddlewares = async (table) => {
  await table.isReady()

  if (TABLES.find(t => table.name == t.name)) {
    return
  }

  TABLES.push(table)

  // геттер списка
  router.get(`/tables/${table.name}`, listGetter(table))

  // добавление элемента
  router.post(`/tables/${table.name}`, elementAdder(table))

  // поиск элемента
  router.get(`/tables/${table.name}/search`, elementSearcher(table))

  // Имя поля для поиска по умолчанию id
  let propertyName = table.name.slice(0,-1) + 'Id'

  // привязка к запросу элемента по уникальному полю
  router.use(`/tables/${table.name}/:${propertyName}`, elementFinder(table, propertyName, 'id'))

  // геттер элемента
  router.get(`/tables/${table.name}/:${propertyName}`, elementGetter(propertyName))

  // изменение элемента
  router.post(`/tables/${table.name}/:${propertyName}`, elementUpdater(table, propertyName))

  // удаление элемента
  router.delete(`/tables/${table.name}/:${propertyName}`, elementDeleter(table, propertyName))
}
