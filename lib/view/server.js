let isStarted = false
let server

module.exports.start = (port, router) => {
  if (isStarted) {
    return server
  }
  isStarted = true

  const express = require('express')
  const app = express()

  // Включаем парсер параметров запроса
  app.use(express.json({ limit: '50mb' }))
  app.use(express.urlencoded({ limit: '50mb', extended: false }))


  // Запускаем прослушивание
  server = app.listen(port)
  console.log(`Запущены маршруты БД, порт ${port}`);

  app.use(router)

  return server
}
