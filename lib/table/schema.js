/**
 * Благодаря данному модулю можно работать с таблицами на уровне кода
 * Создание экземпляра класса Table находит соответствие заданной схемы со схемой в базе данных
 * В случае расхождения схем, производится резервное копирование файла бд и попытка изменить схему в бд под нужную
 */

const assert = require('assert')
const types = require('./table-types')
const wait = require('./../../utils/wait.js')

const {
  selectQueryBuilder,
  selectQuery,
  updateQuery,
  deleteQuery,
  insertQuery
} = require('./query-builder.js')

const TEMP_TABLE_NAME = 'temp_table'



/**
 * Класс схемы таблицы в базе данных.
 */
class Table {
  /**
   * создать таблицу
   * @param {object} table объект с данными таблицы, в том числе
   */
  static async add({ db, table }) {

    let queue = table.sql
    console.log(queue)

    let result = await db.request({ query: queue })
    await db.updateTablesList()

    return result
  }

  /**
   * Переименовать таблицу
   */
  static async rename({ db, oldName, newName }) {
    // ALTER TABLE oldName RENAME TO newName;
    let result = await db.request({ query: `ALTER TABLE ${oldName} RENAME TO ${newName};` })
    await db.updateTablesList()

    return result
  }

  /**
   *  Копирем данные одной таблицы в другую таблицу
   */
  static async copyInto({ db, fromName, fromCols, targetName, targetCols }) {
    let cols = []
    for (let fromCol of fromCols) {
      if (targetCols.includes(fromCol)) {
        cols.push(fromCol)
      }
    }
    console.log(`INSERT INTO ${targetName} (${cols.join(',')}) SELECT ${cols.join(',')} FROM ${fromName};`);
    // INSERT INTO targetName SELECT * FROM fromName;
    let result = await db.request({ query: `INSERT INTO ${targetName} (${cols.join(',')}) SELECT ${cols.join(',')} FROM ${fromName};` })
    await db.updateTablesList()

    return result
  }

  /**
  *  Удалить таблицу
  */
  static async del({ db, tableName }) {
    // DROP TABLE tableName;
    let result = await db.request({ query: `DROP TABLE ${tableName};` })
    await db.updateTablesList()

    return result
  }

  static async addColumn({ db }){
    // ALTER TABLE table1 ADD COLUMN c3 TEXT;
    let result = await db.request({ query: `ALTER TABLE table1 ADD COLUMN c3 TEXT;` })
    await db.updateTablesList()

    return result
  }

  /**
   * @param schema
   * {
   *   columnName: dataType
   * }
   */
  constructor(database, initParameters, schema = {}) {
    if (typeof initParameters == 'string') {
      initParameters = {
        name: initParameters,
        title: initParameters
      }
    }

    // Псевдоним таблицы
    this.title = initParameters.title

    // Привязываем объект соединения с БД
    this.database = database

    // объявляем объект со всеми столбцами таблицы
    this.columns = {}

    // ставим флаг готовности соединения
    this.ready = false

    // проходим по всем стобцам из схемы
    for (let col in schema) {
      // если в качестве значения для столбца строка, считаем, что это тип
      if (typeof schema[col] == 'string')
        schema[col] = { type: schema[col] }

      // достаем тип
      let { type } = schema[col]

      // проверяем, что тип поддерживается
      if (types[type] === undefined)
        throw Error(`Неизвестный тип данных ${type} для столбца таблицы. Используйте один из следующих: ${Object.keys(types)}`)

      // добавлем стобец в список
      this.columns[col] = new types[type](schema[col])
      // console.log(this.columns[col])
    }

    // выполняем связку с таблицей в БД
    this.connect(initParameters.name)
    return this
  }

  // TODO: переименовать
  async isReady() {
    // if (!this.ready) {
    //   console.log('Подключение к таблице подготавливается')
    //   return new Promise( (res, rej) => setTimeout(_ => res(this.isReady), 50) )
    // }
    return wait(_ => this.ready)
  }

  /*
   * присоединение созданного объекта к опеределенной таблице в БД
   */
  async connect(name) {

    // ожидаем готовности подключения
    await this.database.isReady()

    // запоминаем имя
    this.name = name


    // проверяем, что таблица с указанным именем существует, в противном случае создаем данную таблицу
    try {
      this.database.checkTable(name)
    } catch (e) {
      console.log('Добавляем таблицу в БД')
      try {
        let result = await Table.add({ db: this.database, table: this })
        console.log('таблица добавлена', result)
      } catch (e) {
        console.log('Добавить не удалось', e)
      }
    }

    let tableFromDb = this.database.checkTable(name)

    // удостоверяемся, что столбцы в одной и другой таблице совпадают
    await this.compare(tableFromDb)

    // Устанавливаем готовность
    this.ready = true
    return this
  }

  /**
  * метод для сверения таблицы в базе данных с заданной схемой
  */
  async compare(table) {
    if (table.sql === this.sql) {
      return true
    }

    //
    console.info(`Некоторые столбцы ${this.name} изменены`)

    // Пробуем исправить
    try {
      // Переименовываем таблицу
      console.log(await Table.rename({ db: this.database, oldName: this.name, newName: `temp_${this.name}` }))

      // Создаем новую таблицу
      console.log(await Table.add({ db: this.database, table: this }))

      // Копируем данные из старой в новую
      console.log(await Table.copyInto({
        db: this.database,
        fromName: `temp_${this.name}`,
        fromCols: table.columns.map(c => c.name),
        targetName: this.name,
        targetCols: Object.keys(this.columns),
      }))

      // Удаляем временную таблицу
      console.log(await Table.del({ db: this.database, tableName: `temp_${this.name}` }))

      let tableFromDb = this.database.checkTable(this.name)

      console.info('Таблица в БД:\n%s', tableFromDb.sql)
      console.info('Модель таблицы:\n%s', this.sql)
    } catch (e) {
      console.error(e)

      throw e
    }

    return false
  }

  /**
   * Вычисляемое свойство, которое на основе данных объекта составляет соответствующий SQL-запрос
   */
  get sql() {
    let queue = `CREATE TABLE "${this.name}" (`

    let _queue = []
    for (let c in this.columns) {
      _queue.push(`\n\t"${c}"\t${this.columns[c].sql()}`)
    }

    queue += _queue.join(',') + `\n)`

    // console.log(queue)
    return queue
  }

  // TODO: Добавить OFFER и LIMIT
  /**
   * Поиск определенной записи
   */
  async get(search) {
    await this.isReady()

    // let { cols, table, joins, searchs } = selectQueryBuilder(this, search)
    // // преобразуем к строке нужного формата
    // cols = cols.join(', ')
    // joins = joins.length ? ' INNER ' + joins.join(' ') : ''
    // searchs = searchs.length ? ' WHERE ' + searchs.join(' and ') : ''
    //
    // let query = `SELECT ${cols}\nFROM ${table}${joins}${searchs}`
    const query = selectQuery(this, search)

    // делаем запрос
    // console.log(`\nQUERY:\n${query}`)
    let rows = await this.database.request({ table: this.name, query })

    // видоизменяем результат в соответствии с типами
    for (let row of rows) {
      for (let col in row) {
        if (this.columns[col] !== undefined) {
          // console.log(col)
          row[col] = this.columns[col].loadHandler(row[col])
        }

        if (col.includes('_')) {
          let name = col.split('_')[0]
          let key = col.split('_')[1]
          row[name] = row[name] || {}
          row[name][key] = row[col]
          delete row[col]
        }
      }

    }

    return rows
  }

  // TODO: Реализовать собственным запросом
  /**
  * Поиск определенной записи
  */
  async getOne(search) {
    let arr = await this.get(search)

    if (arr.length === 0)
      throw Error('Запись не найдена')

    return arr[0]
  }

  // TODO: добавить адекватный ответ с результатом выполнения
  /**
  * Вставка новой записи
  */
  async insert(row) {
    await this.isReady()
    // return await this.database.insert({ table: this.name, row })

    const query = insertQuery(this, row)

    // делаем запрос
    // console.log(`\nQUERY:\n${query}`)
    let rows = await this.database.request({ table: this.name, query })

    return rows
  }

  // TODO: добавить адекватный ответ с результатом выполнения
  /**
  * Обновление записи
  */
  async update({ id, search = { id }, updatedData  }) {
    await this.isReady()

    const query = updateQuery(this, search, updatedData)

    // делаем запрос
    let rows = await this.database.request({ table: this.name, query })

    return rows
  }

  // TODO: добавить адекватный ответ с результатом выполнения
  /**
  * Удаление записи
  */
  async delete(search) {
    await this.isReady()
    // return await this.database.delete({ table: this.name, value: id, search })

    const query = deleteQuery(this, search)

    // делаем запрос
    // console.log(`\nQUERY:\n${query}`)
    let rows = await this.database.request({ table: this.name, query })

    return rows
  }
}


module.exports = Table
