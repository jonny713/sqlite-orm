// const Table = require('./schema.js')

// функция для экранирования строк
function esc(str) {
  return str.replace(/\"/g, '\\"')
}

// TODO: Зачем нам тут массив таблиц? Зачем отдельно имя таблицы? Может лучше передавать непосредственно объект таблицы и в нем хранить имя? Разобраться
// TODO: Лучше возвращать некий итоговый результат, нежели набивать передаваемые аргументы данными
/**
 * Рекурсивное создание параметров запроса SELECT и join'ы других таблиц
 * @param {string} table имя таблицы
 * @param {Array} tables массив таблицы
 * @param {object} row объект с параметрами запроса
 * @param {Array} cols массив, в который будут помещены итоговые результаты
 * @param {Array} joins массив, в который будут помещены join'ы других таблиц
 * @param {Array} searchs массив, в который будут помещены where'ы
 */
function _selectQueryBuilder(table, tables, row, cols = [], joins = [], searchs = []) {
  for (let header of tables[table].columns) {
    if (typeof row[header.name] == 'string')
      searchs.push(`${table}.${header.name}="${esc(row[header.name])}"`)

    if (typeof row[header.name] == 'number' && typeof row[header.name] == 'boolean')
      searchs.push(`${table}.${header.name}=${row[header.name]}`)

    if (header.name.includes('Id')) {
      let joinedTable = header.name.replace('Id', '')
      joins.push(`JOIN ${joinedTable} ON ${table}.${header.name}=${joinedTable}.id`)
      if (typeof row[joinedTable] == 'object') {
        cols.push(...tables[joinedTable].columns.map(h => `${joinedTable}.${h} as ${joinedTable}_${h}`))
        _selectQueryBuilder(joinedTable, tables, row[joinedTable], cols, joins, searchs)
      }
    }
  }
}

/**
 * ff
 * @param {Table} tableObject
 * @param {object} _search
 * @param {string[]} viewed
 * @return {{ cols: string[], table: string, joins: string[], searchs: string[] }}
 */
function selectQueryBuilder(tableObject, _search = {}, viewed = [tableObject.name]) {
  // массив строк с запращиваемыми столбцами
  let cols = []
  // имя таблицы
  let table = tableObject.name
  // массив строк с JOIN'ами
  let joins = []
  // массив поиска
  let searchs = []


  // производим преобразования в соответствии с типами
  for (let col in tableObject.columns) {
    // console.log(tableObject.name, col, _search);
    let { column, search, joined } = tableObject.columns[col].searchHandler(_search[col])

    // добавляем новую строку, если тип позволяет
    if (column !== undefined) {
      cols.push(`${table}.${col}`)
    }

    if (search !== undefined) {
      searchs.push(`${table}.${col}=${search}`)
    }

    if (joined !== undefined && joined instanceof tableObject.constructor && !viewed.includes(joined.name)) {
      viewed.push(joined.name)
      let { cols: _cols, table: _table, joins: _joins, searchs: _searchs } = selectQueryBuilder(joined, _search[joined.name], viewed)

      joins.push(`JOIN ${_table} ON ${table}.${col}=${_table}.id`)

      cols.push(..._cols.map(h => `${h} as ${_table}_${h.replace(_table + '.', '')}`))
      searchs.push(..._searchs)
      joins.push(..._joins)
    }
  }

  return { cols, table, joins, searchs }
}

function updateQueryBuilder(tableObject, data) {
  // массив строк с запращиваемыми столбцами
  let cols = []
  // массив строк с запращиваемыми столбцами
  let values = []
  // массив строк с изменяемыми столбцами
  let sets = []

  // производим преобразования в соответствии с типами
  for (let col in tableObject.columns) {
    // Пропускаем, если поле не обновляется
    if (!(col in data)) {
      continue
    }

    // console.log(tableObject.name, col, _search);
    let { column, save } = tableObject.columns[col].saveHandler(data[col])

    // добавляем новую строку, если тип позволяет
    if (column) {
      cols.push(col)
      values.push(save)
      sets.push(`${col}=${save}`)
    }
  }

  return { sets, cols, values }
}



function selectQuery(tableObject, search) {

  let { cols, table, joins, searchs } = selectQueryBuilder(tableObject, search)
  // преобразуем к строке нужного формата
  cols = cols.join(',\n\t')
  joins = joins.length ? '\n' + joins.join('\n ') : ''
  searchs = searchs.length ? '\nWHERE\n\t ' + searchs.join(' and\n\t ') : ''

  return `SELECT\n\t${cols}\nFROM ${table}${joins}${searchs}`
}

function updateQuery(tableObject, search, data) {

  let { sets } = updateQueryBuilder(tableObject, data)

  if (sets.length === 0) {
    throw new Error('Отсутствую поля, доступные для обновления')
  }

  let { joins, searchs } = selectQueryBuilder(tableObject, search)
  joins = joins.length ? ' ' + joins.join(' ') : ''
  searchs = searchs.length ? ' WHERE ' + searchs.join(' and ') : ''

  return `UPDATE ${tableObject.name} SET ${sets.join(', ')} WHERE id in (SELECT ${tableObject.name}.id\nFROM ${tableObject.name}${joins}${searchs})`
}

function deleteQuery(tableObject, search) {
  // TODO: Возможно, стоит как-то предотвратить массовое удаление записей

  let { joins, searchs } = selectQueryBuilder(tableObject, search)
  joins = joins.length ? ' ' + joins.join(' ') : ''
  searchs = searchs.length ? ' WHERE ' + searchs.join(' and ') : ''

  return `DELETE FROM ${tableObject.name} WHERE id in (SELECT ${tableObject.name}.id\nFROM ${tableObject.name}${joins}${searchs})`
}

function insertQuery(tableObject, data) {
  // формируем массивы столбцов и значений для них
  let { cols, values } = updateQueryBuilder(tableObject, data)

  return `INSERT INTO ${tableObject.name} (${cols.join(',')}) VALUES (${values.join(',')})`
}


module.exports = {
  selectQueryBuilder,
  _selectQueryBuilder,
  selectQuery,
  updateQuery,
  deleteQuery,
  insertQuery,
  esc
}
