
class TableType {
  constructor({ sqlType = 'INTEGER',  primaryKey,  notNull,  autoincrement,  unique,  def = null }) {
    this.sqlType = sqlType
    this.primaryKey = !!primaryKey
    this.notNull = !!notNull
    this.autoincrement = !!autoincrement
    this.unique = !!unique
    this.default = def

    return
  }

  /**
  * выдает отформатированную строку с sql типом и параметрами
  * в качестве входных данных получает переопределения для параметров для данного запроса
  */
  sql() {
    let sql = this.sqlType

    if (this.notNull)
      sql += ' NOT NULL'
    if (this.primaryKey)
      sql += ' PRIMARY KEY'
    if (this.primaryKey && this.autoincrement)
      sql += ' AUTOINCREMENT'
    if (this.unique)
      sql += ' UNIQUE'
    if (this.default !== undefined && this.default !== null)
      sql += ' DEFAULT ' + this.saveHandler(this.default) //(typeof this.default == 'object' ? '\'' + JSON.stringify(this.default) + '\'' : this.default)

    return sql
  }

  /*
  */

  search(){}


  /**
  * Обрабатывает значение, по которому производится поиск в БД
  *
  * @param data - данные, по которым будет производится поиск
  *
  * @returns {{column: Boolean, joined: Table, search: any}}
  * column - включать ли в поисковую выдачу значение конкретного поля
  * joined - ссылка на связанную таблицей, если такая связь имеется
  * search - преобразованные данные для поиска
  */
  searchHandler(data) {
    return {
      column: true,
      search: data
    }
  }

  /**
  * Обрабатывает значение, полученное из БД
  *
  * @param data - данные, полученные из БД
  *
  * @returns data - преобразованные, согласно их типу данные
  */
  loadHandler(data) {
    return data
  }

  /**
  * Обрабатывает значение перед сохранением
  *
  * @param data - данные, которые нужно сохранить
  *
  * @returns {column: Boolean, save: any}
  * column - включать ли в поисковую выдачу значение конкретного поля
  * save - преобразованные, согласно их типу, данные
  */
  saveHandler(data) {
    return {
      column: true,
      save: data
    }
  }
}

module.exports = TableType
