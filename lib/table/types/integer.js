const TableType = require('./type')

class INTEGER extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'

    // Данные параметры переопределять можно
    let def = typeof schema.default === 'number' ? schema.default : null

    return super({ ...schema, sqlType, def })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    if (typeof s.search == 'number' || !isNaN(data) && data !== '') {
      s.search = `${parseInt(data)}`
    } else {
      delete s.search
    }

    return s
  }

  saveHandler(data) {
    let s = super.saveHandler(data)
    console.log(!isNaN(data));
    if (typeof s.save == 'number' || !isNaN(data) && data !== '') {
      s.save = `${parseInt(data)}`
    } else {
      s.column = false
    }

    return s
  }
}

module.exports = INTEGER
