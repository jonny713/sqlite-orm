const TableType = require('./type')

class BOOLEAN extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'

    // Данные параметры переопределять можно
    let def = +!!schema.default

    return super({ ...schema, sqlType, def })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    if (typeof data == 'boolean') {
      s.search = s.search === 'true' ? true : s.search === 'false' ? false : +!!data
    } else {
      delete s.search
    }

    return s
  }

  loadHandler(data) {
    return !!data
  }

  saveHandler(data) {
    let s = super.saveHandler(data)

    if ([true, false, 'true', 'false', 0, 1].includes(data)) {
      s.save = s.save === 'false' ? false : +!!data
    } else {
      s.column = false
    }

    return s
  }
}

module.exports = BOOLEAN
