const TableType = require('./type')

class REFERENCE extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'

    // Данные параметры переопределять можно
    let def = typeof schema.default == 'number' ? schema.default : null

    super({ ...schema, sqlType, def })

    this.refTable = schema.table
    return this
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    delete s.column
    delete s.search

    s.joined = this.refTable

    return s
  }

  saveHandler(data) {
    let s = super.saveHandler(data)

    if (typeof s.save == 'number' || !isNaN(data) && data !== '') {
      s.save = `${parseInt(data)}`
    } else {
      s.column = false
    }

    return s
  }
}

module.exports = REFERENCE
