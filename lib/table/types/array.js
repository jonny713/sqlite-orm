const TableType = require('./type')

class ARRAY extends TableType {
  constructor(schema) {
    // Данные параметры нельзя переопределять
    let sqlType = 'TEXT'

    // Данные параметры переопределять можно
    let def = Array.isArray(schema.default) ? schema.default : []

    super({ ...schema, sqlType, def })

    return this
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    delete s.search

    return s
  }

  loadHandler(data) {
    if (typeof data == 'string' && data[0] == '[') {
      try {
        let res = JSON.parse(data)
        return res
      } catch (e) {
        console.error('Не удалось распарсить значение')
        return []
      }
    }
    console.log(`Формат значения ${data} не соотвествует типу ARRAY`)
    return []
  }

  saveHandler(data) {
    let s = super.saveHandler(data)

    if (typeof s.save == 'object' && Array.isArray(s.save)) {
      try {
        s.save = `'${JSON.stringify(data)}'`
        return s
      } catch (e) {
        console.error('Не удалось преобразовать в JSON')
        s.column = false
        return s
      }
    }

    console.log(`Формат значения ${data} не соотвествует типу ARRAY`)
    s.column = false
    return s
  }
}

module.exports = ARRAY
