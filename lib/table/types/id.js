const TableType = require('./type')

class ID extends TableType {
  constructor() {
    // Данные параметры нельзя переопределять
    let sqlType = 'INTEGER'
    let primaryKey = true
    let notNull = true
    let autoincrement = true
    let unique = true

    return super({ sqlType, primaryKey, notNull, autoincrement, unique })
  }

  searchHandler(data) {
    let s = super.searchHandler(data)

    if (typeof s.search == 'number') {
      s.search = `${data}`
    } else if (!isNaN(data)) {
      s.search = `${parseInt(data)}`
    } else if (typeof s.search == 'string') {
      s.search =`0`
    } else {
      delete s.search
    }
    // delete s.column

    return s
  }


  saveHandler(data) {
    let s = super.saveHandler(data)

    s.column = false
    return s
  }
}

module.exports = ID
