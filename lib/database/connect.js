const sqlite = require('sqlite3').verbose()
// const { selectQueryBuilder, esc } = require('./query-builder.js')
const DB_NAME = ':memory:'

/**
 *
 * Класс предназначенный для работы непосредственно с подключением к базе данны
 * Работает по принципу "синглтона"
 */
class DB {
  /**
   * Создает подключение к указанной таблице или возвращает ранее созданное
   * @param {string} dbName - имя файла базы данных (по умолчанию создается бд в оперативной памяти)
   */
  static connect({ dbName = DB_NAME } = {}) {
    // Проверяем, что поле all существует и ищем там ранее созданное подключение к бд
    if (DB.all && DB.all[dbName])
      return DB.all[dbName]

    // Создаем подключение
    let db = new DB(dbName)

    // Сохранение подключения в списке
    DB.all = DB.all || {}
    DB.all[dbName] = db

    return db
  }

  constructor(dbName) {
    this.ready = false
    this.init(dbName)
    return this
  }

  /**
   * Метод-костыль для ожидания готовности подключения
   */
  async isReady() {
    if (!this.ready) {
      console.log('Подключение к БД подготавливается')
      return new Promise( (res, rej) => setTimeout(_ => res(this.isReady), 50) )
    }
  }

  /*
   * Актуализируем список таблиц в БД со списком в объекте
   */
  async updateTablesList() {
    try {
      // Достаем из бд список всех имеющихся таблиц
      let tables = await new Promise( (res, rej) => this._db.all(`SELECT * FROM SQLITE_MASTER`, (err, rows) => err ? rej(err) : res(rows) ) )
      tables = tables.filter(t => t.type == 'table')

      this.tables = {}
      for (let table of tables) {
        try {
          this.tables[table.name] = {
            columns:  table.sql.replace(/[^(]+/, '').match(/"[^"]+"[^")]+/g).map(s => ({
              name: s.match(/"([^"]+)"/)[1],
              type: s.match(/"\s([^\s,]+)[\s,]/)[1],
              primaryKey: s.includes('PRIMARY KEY'),
              notNull: s.includes('NOT NULL'),
              autoincrement: s.includes('AUTOINCREMENT'),
              unique: s.includes('UNIQUE'),
              default: s.includes('DEFAULT') ? s.replace(/.*(DEFAULT (\d+)|DEFAULT '([^']+)')[\s\S]*/, '$2$3') : undefined
            })),
            sql: table.sql
          }
        } catch (e) {
          console.log(`${table.name} проигнорирована по причине - ${e.message}`)
        }
      }
    } catch (e) {
      console.error(e)
    }
  }

  /**
   * Инициализация подключения к файлу базы данных
   */
  async init(dbName){
    try {
      // создаем подключение
      this._db = await new Promise( (res, rej) => {
        let db = new sqlite.Database(dbName, err => err ? rej(err) : res(db))
      })

      await this.updateTablesList()

      this.ready = true
      console.log('Соединение с базой данных успешно установлено')

      // возвращаем
      return this
    } catch (e) {
      console.log('Соединение с базой данных установит не удалось')
      console.error(e)
      return
    }
  }

  /**
   * Проверка наличия таблицы
   */
  checkTable(table) {
    // Ищем таблицу среди имеющихся
    if (!(table in this.tables)) {
      console.log(`Таблицы с именем ${table} не существует`)
      throw Error(`Таблицы с именем ${table} не существует`)
    }
    return this.tables[table]
  }

  /**
   * Выполнение свободного SQL-запроса
   */
  async request({ query = '' }) {
    // ожидаем готовности подключения
    await this.isReady()

    console.log(`\nQUERY:${query}`)
    // let result = await new Promise( (res, rej) => this._db.run(query, (err, rows) => err ? rej(err) : res(rows) ) )
    //
    let result
    if (query.split(' ')[0] == 'UPDATE' || query.split(' ')[0] == 'INSERT') {
      result = await new Promise( (res, rej) => this._db.run(query, function(err, rows) {
        return err ? rej(err) : res(this.lastID)
      }))
    } else {
      result = await new Promise( (res, rej) => this._db.all(query, (err, rows) => err ? rej(err) : res(rows) ) )
    }
    console.log(`----\n `, Array.isArray(result) ? result : `Last id is ${result}`)
    console.log(`----\n`)

    return result
  }

}

module.exports = DB.connect
